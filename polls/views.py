from django.http import HttpResponse
from django.shortcuts import render, redirect
# Create your views here.
from polls import models


def home(req):
    return HttpResponse("Hello World")

def list(req):
    questions = models.Question.objects.all()
    context = {}
    context["questions"] = questions
    context["hello"] = "this is hello"
    return render(req, "polls/home.html", context)

def detail(req, id):
    question = models.Question.objects.get(id=id)
    question.views += 1
    question.save()
    context = {"question":question}
    return render(req, "polls/detail.html", context)

def vote(req, id):
    # increase vote count
    print("question id", id)
    choice_id = req.POST["choice"]
    print("selected choice id", choice_id)

    question = models.Question.objects.get(id=id)
    choice = question.choice_set.get(id=choice_id)

    choice.vote += 1
    choice.save()

    # return redirect("/")
    return redirect("/polls/" + str(id) + "/")
    # return redirect("/polls/{}/".format(id))
