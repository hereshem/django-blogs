from django.contrib import admin

# Register your models here.
from polls import models

class ChoiceInline(admin.TabularInline):
    model = models.Choice
    extra = 1
    fields = ["text"]

class QuestionAdmin(admin.ModelAdmin):
    # list pages
    list_display = ["text", "created", "modified", "views"]
    search_fields = ["text", "views"]
    list_filter = ["created"]

    # detail page
    inlines = [ChoiceInline]
    # fields = ["text"]
    fieldsets = [
        ["Description", {'fields': ['text']}],
        # ['Date information', {'fields': ['created', "modified"]}],
    ]

admin.site.register(models.Question, QuestionAdmin)
# admin.site.register(models.Choice)