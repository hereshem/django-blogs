from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect


# Create your views here.
from blogs.forms import ReviewForm, UserSignupForm, LoginForm
from blogs.models import Article


def list(req):
    datas = Article.objects.filter(published=True)
    return render(req, "blogs/index.html", {"datas":datas})

def search(req):
    q = req.GET["q"]
    datas = Article.objects.filter(published=True, title__contains=q)
    return render(req, "blogs/index.html", {"datas":datas})

def detail(req, id):
    article = Article.objects.get(id=id)
    article.view += 1
    article.save()
    form = ReviewForm()
    return render(req, "blogs/detail.html", {"article":article,"form":form})

def review(req, id):
    if req.method == "POST":
        form = ReviewForm(req.POST)
        myreview = form.save(commit=False)
        if myreview.rate <= 5 and myreview.rate >= 1:
            myreview.article = Article.objects.get(id=id)
            myreview.user = req.user
            myreview.save()


    return redirect("/blogs/%d/" % id)

def mylogin(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(username=username, password=password)
        if user:
            # check for user startus if user.is
            login(request, user)
            return redirect("/blogs/")
    else:
        form = LoginForm()
    return render(request, "blogs/login.html", {"form":form})

def mylogout(request):
    logout(request)
    return redirect("/blogs/")

def mysignup(request):

    if request.method == "POST":
        form = UserSignupForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            # procces for initial user
            user.save()
            return redirect("/blogs/")

    else:
        form = UserSignupForm()
    return render(request, "blogs/signup.html", {"form":form})
